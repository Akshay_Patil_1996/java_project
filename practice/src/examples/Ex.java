package examples;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

public class Ex {

	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter Size.....");
		int size = Integer.parseInt(br.readLine());
		int fact = 1;
		for (int i = 1; i <= size; i++) {

			fact = fact * i;
			
		}

		System.out.println("" + fact);
	}

}
