package StringDemo;

public class str3Hashcode {
	public static void main(String[] args) {

		String str1 = "Akshay";
		String str2 = new String("Akshay");

		System.out.println(str1);
		System.out.println(str1.hashCode());
		System.out.println(str2);
		System.out.println(str2.hashCode());

		if (str1.equals(str2)) {
			System.out.println("Equal");

		} else {
			System.out.println("Not Equal");

		}

	}

}
