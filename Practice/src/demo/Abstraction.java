package demo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

abstract class Employee {

	int paymentPerHours = 100;
	final int totalWorkingHours = 8;
	int TotalDays;
	int SalaryPerDay;
	int SalaryPerMonth;

	abstract void calculateSalary() throws Exception, IOException;
}

class salary extends Employee {

	void calculateSalary() throws Exception, IOException {

		int SalaryPerDay = totalWorkingHours * paymentPerHours;
		System.out.println("Salary of employee Per Day= " + SalaryPerDay);

	}

}

class FullTimeEmployee extends Employee {

	void calculateSalary() throws Exception, IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter total working Days of full time Employee =");
		TotalDays = Integer.parseInt(br.readLine());
		SalaryPerDay = totalWorkingHours * paymentPerHours;
		SalaryPerMonth = TotalDays * SalaryPerDay;
		System.out.println("Salary of employee is= " + SalaryPerMonth);

	}

}

public class Abstraction {

	public static void main(String[] args) throws IOException, Exception {

		Employee e = new salary();
		e.calculateSalary();
		Employee e1 = new FullTimeEmployee();
		e1.calculateSalary();

	}

}
