package demo;

class BowlerClass {

	String speed;
	String type, action;

	void bowlingMethod() {

		System.out.println("Bowlers attributes");

	}

}

class FastPacer extends BowlerClass {

	void bowlingMethod() {

		speed = "135 to above";
		type = "1.Left Arm 2.Right Arm";
		action = "1.Inswing 2.Outswing 3.Reverse swing 4.FUll toss 5. Yorker";
		System.out.println("Speed= " + speed);
		System.out.println("Type= " + type);
		System.out.println("Action= " + action);

	}
}

class MediumPacer extends BowlerClass {

	void bowlingMethod() {

		speed = "120 - 150";
		type = "1.Left Arm 2.Right Arm";
		action = "1.Inswing  2.Outswing  3.Reverse swing 4.FUll toss 5. Yorker";
		System.out.println("Speed= " + speed);
		System.out.println("Type= " + type);
		System.out.println("Action= " + action);

	}

}

class Spinner extends BowlerClass {

	void bowlingMethod() {

		speed = "100 - 125";
		type = "1.Left Arm spinner  2.Right Arm spinner";
		action = "1.Googly  2.Flat delivery  3.FUll toss";
		System.out.println("Speed= " + speed);
		System.out.println("Type= " + type);
		System.out.println("Action= " + action);

	}

}

public class Polymorphism {

	public static void main(String[] args) {

		BowlerClass f = new FastPacer();
		f.bowlingMethod();
		BowlerClass m = new MediumPacer();
		m.bowlingMethod();
		BowlerClass s = new Spinner();
		s.bowlingMethod();
	}
}
