package demo;

abstract class bike {
	public abstract void run();
}

public class Prac1 extends bike {

	public void run() {
		System.out.println("RUN");

	}

	public static void main(String[] args) {

		Prac1 p = new Prac1();
		p.run();

	}
}
