package demo;

interface Bank {

	void rateOfInterest();

	double amount = 1000;
}

class AxisBank implements Bank {

	double interest = 0;
	double rateOfInterest;

	public void rateOfInterest() {

		interest = 5;

		rateOfInterest = amount * interest / 100;
		System.out.println("Axis Bank Interest " + rateOfInterest);

	}

}

class SBIBank implements Bank {

	double interest = 0.0;
	double rateOfInterest;

	public void rateOfInterest() {

		interest = 5.5;

		rateOfInterest = amount * interest / 100;
		System.out.println("SBI Bank Interest " + rateOfInterest);

	}

}

class BankOfMaharashtraBank implements Bank {

	double interest = 0;
	double rateOfInterest;

	public void rateOfInterest() {

		interest = 6.4;

		rateOfInterest = amount * interest / 100;
		System.out.println("Bank Of Maharashtra Bank Interest " + rateOfInterest);

	}

}

class ICICIBank implements Bank {

	double interest = 0;
	double rateOfInterest;

	public void rateOfInterest() {

		interest = 7.1;

		rateOfInterest = amount * interest / 100;
		System.out.println("ICICI Bank Interest " + rateOfInterest);

	}

}

class HDFCBank implements Bank {

	double interest = 0;
	double rateOfInterest;

	public void rateOfInterest() {

		interest = 5;

		rateOfInterest = amount * interest / 100;
		System.out.println("HDFC Bank Interest " + rateOfInterest);

	}

}

public class InterfaceDemo {

	public static void main(String[] args) {
		AxisBank ab = new AxisBank();
		ab.rateOfInterest();
		SBIBank s = new SBIBank();
		s.rateOfInterest();
		BankOfMaharashtraBank b = new BankOfMaharashtraBank();
		b.rateOfInterest();
		ICICIBank i = new ICICIBank();
		i.rateOfInterest();
		HDFCBank h = new HDFCBank();
		h.rateOfInterest();

	}

}
