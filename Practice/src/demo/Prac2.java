package demo;

abstract class shape {
	abstract void draw();
}

class circle extends shape {
	void draw() {
		System.out.println("Circle");
	}
}

class Rect extends circle {
	void draw() {
		System.out.println("Rect");

	}
}

public class Prac2 {
	public static void main(String[] args) {
		shape r = new Rect();
		circle c = new Rect();
		r.draw();
		c.draw();
	}

}
