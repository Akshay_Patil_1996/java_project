package demo;

public class Encapsulation {

	private int SSN=0;

	public int getSSN() {
		return SSN;

	}

	public void setSSN(int SSN) {

		this.SSN = SSN;
		System.out.println(this.SSN);

	}

	public static void main(String[] args) {

		Encapsulation e = new Encapsulation();
		System.out.println(e.getSSN());
		e.setSSN(10);

	}

}
