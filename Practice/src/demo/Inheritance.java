package demo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

class account {

	int deposit;
	int withdraw;
	static int currentBalance = 0;

	public void deposit(int deposit) {

		currentBalance = currentBalance + deposit;
		System.out.println("Current Balance = " + currentBalance);

	}

	void withdraw(int withdraw) {
		currentBalance = currentBalance - withdraw;
		System.out.println("Current Balance after withdrawel = " + currentBalance);
	}

}

class User1 extends account {

	public void deposit() throws NumberFormatException, IOException {

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter amount to deposit = ");
		deposit = Integer.parseInt(br.readLine());
		account a = new account();
		a.deposit(deposit);

	}

	public void withdrow() throws NumberFormatException, IOException {

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter amount to withdraw = ");
		withdraw = Integer.parseInt(br.readLine());
		account a = new account();
		a.withdraw(withdraw);

	}

}

public class Inheritance {
	public static void main(String[] args) throws NumberFormatException, IOException {
		User1 u = new User1();
		u.deposit();
		u.withdrow();
	}

}
