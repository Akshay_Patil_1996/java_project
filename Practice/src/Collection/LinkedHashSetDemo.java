package Collection;

import java.util.LinkedHashSet;

public class LinkedHashSetDemo {

	// LinkedHashSet is a class.
	// It is a implementation of Hashtable and Linked list of the set interface
	// It inherits HashSet class and implements Set interface.

	// The important points about Java LinkedHashSet class are:
	//
	// Java LinkedHashSet class contains unique elements only like HashSet.
	// Java LinkedHashSet class provides all optional set operation and permits
	// null elements.
	// Java LinkedHashSet class is non synchronized.
	// Java LinkedHashSet class maintains insertion order.

	public static void main(String[] args) {

		LinkedHashSet<String> lhset = new LinkedHashSet<String>();
		lhset.add("Akshay");// add elements
		lhset.add("Patil");// add elements

		lhset.add(null);// null
		lhset.add("Remove");// add elements
		lhset.add("Remove1");// add elements

		System.out.println(lhset);
		lhset.remove("Remove1");// remove elements
		System.out.println(lhset);
	}
}
