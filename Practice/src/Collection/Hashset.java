package Collection;

import java.util.HashSet;

public class Hashset {

	// HashSet is a class
	// Used to create a collection that uses a hash table for storage
	// Inherits the AbstractSet class
	// Implements Set interface

	// Important Points---
	// HashSet stores the elements by using a mechanism called hashing
	// HashSet contains unique elements only.
	// HashSet allows null value.
	// HashSet class is non synchronized.
	// HashSet doesn't maintain the insertion order.
	// Elements are inserted on the basis of their hashcode.
	// Best approach for search operations.
	// The initial default capacity of HashSet is 16, and the load factor is
	// '0.75'.

	// Difference between List and Set is A list can contain duplicate elements
	// whereas Set contains unique elements only.

	public static void main(String[] args) {

		HashSet<String> set = new HashSet<String>();
		set.add("Akshay");// add elements
		set.add("Patil");// add elements
		set.add(null);// null
		set.add("Remove");// add elements
		set.add("Remove1");// add elements

		System.out.println(set);
		set.remove("Remove1");// remove elements
		System.out.println(set);

	}
}
