package Collection;

import java.awt.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;

public class Collection_3 {

	public static void main(String[] args) {

		ArrayList<String> list1 = new ArrayList<>();
		list1.add("Akshay ");
		list1.add("Patil ");

		LinkedList<String> li = new LinkedList<String>();
		li.add("Akshay");
		li.add("Patil");

		Iterator itr = li.iterator();

		System.out.println(list1);
		System.out.println(li);
		while (itr.hasNext()) {
			System.out.println(itr.next());

		}

	}

}
