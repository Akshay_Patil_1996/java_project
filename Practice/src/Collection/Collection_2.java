package Collection;

import java.awt.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;

public class Collection_2 {

	public static void main(String[] args) {

		LinkedList<String> li = new LinkedList<String>();
		li.add("Akshay");
		li.add("Patil");

		Iterator itr = li.iterator();

		while (itr.hasNext()) {
			System.out.println(itr.next());

		}

	}

}
